package com.shuvi.cinema.controller.dto.notification;

import lombok.Builder;
import lombok.Data;


/**
 * @author Shuvi Dola
 *
 * Запрос на рассылку уведомлений.
 */
@Data
@Builder
public class NotificationRequest {

    private String message;
    private String to;
}
