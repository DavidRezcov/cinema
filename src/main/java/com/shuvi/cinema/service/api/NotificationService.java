package com.shuvi.cinema.service.api;


import com.shuvi.cinema.controller.dto.notification.NotificationRequest;

/**
 * @author Shuvi Dola
 *
 * Сервис рассылки уведомлений.
 */
public interface NotificationService {


    /**
     * Отправка уведомления.
     *
     * @param notificationRequest Запрос на создание уведомления.
     */
    void sendNotification(NotificationRequest notificationRequest);
}
